#!/usr/bin/env python
from mwclient import Site
from argparse import ArgumentParser
import pprint, json

# SCRIPT:
# get all the author in wiki
# get all their contributions

#####
# Args
####
p = ArgumentParser()
p.add_argument('-V', '--version', action='version', version='version 1.0')
p.add_argument("--host", default="beyond-social.org")
p.add_argument("--path", default="/wiki/", help="path: should end with /")
site = Site(host='beyond-social.org', path='/wiki/')

# dict: users articles
contributions = { "nodes": [], "links": []}


# find all users
users = [i['name'] for i in list(site.allusers())]

# get users contributions
for username in users:
    contributions['nodes'].append({"name": username, "group": "author"})
    source = len(contributions['nodes']) - 1 

    user = site.usercontributions(username)
    user_contributions = [edit['title'] for edit in user if 
                     'File:' not in edit['title'] 
                     and 'Category:' not in edit['title']   
                     and 'Template:' not in edit['title']   
                     and 'Widget:' not in edit['title']   
                     and 'Talk:' not in edit['title']   
                     and 'User:' not in edit['title']
                     and 'User talk:' not in edit['title']                            
                     and 'Mediawiki:' not in edit['title']                        
                     and 'Special:' not in edit['title']                        
                     ] #purge pages in other Namespaces than Main
    user_contributions= list(set(user_contributions) ) # rm duplicated
    for page in user_contributions:
        contributions['nodes'].append({"name": page, "group": "article"})
        target = len(contributions['nodes']) - 1 
        contributions['links'].append({"source":source, "target":target})

## links
# user: username (source)
# all contributions user:  user_contributions (target)
# positions of these two in nodes list



#pprint.pprint(contributions)

jsonfile = open('contributions.json', 'w')
json.dump(contributions, jsonfile, indent = 4 )

# create json
# * start with a dictionary

# {u'comment': u'', u'pageid': 520, u'title': u'Retour Afzender', u'timestamp': time.struct_time(tm_year=2015, tm_mon=6, tm_mday=27, tm_hour=10, tm_min=34, tm_sec=26, tm_wday=5, tm_yday=178, tm_isdst=-1), u'top': u'', u'userid': u'1', u'revid': 2905, u'user': u'Andre', u'parentid': 2901, u'ns': 0, u'size': 3513}


